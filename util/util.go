package util

import (
	"os/exec"
	"strings"
)

func Execute(command string, args string) error {
	return exec.Command(command, Split(args)...).Run()
}

func ExecuteSpace(command string, args string) error {
	return exec.Command(command, SplitSpace(args)...).Run()
}

func Split(in string) []string {
	return strings.Split(in, "")
}

func SplitSpace(in string) []string {
	return strings.Split(in, " ")
}

func ServiceNameFromDir(s string) string {
	split := strings.Split(s, "/")
	return split[len(split)-1]
}
