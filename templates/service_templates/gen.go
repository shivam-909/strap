package service_templates

import (
	"fmt"
	"go/format"
)

func GenerateBackends(name string) string {
	b, err := format.Source([]byte(fmt.Sprintf(backends, name)))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateTest() string {
	b, err := format.Source([]byte(test))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateLogic() string {
	b, err := format.Source([]byte(logic))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateApiProto(name string, service string) string {
	return fmt.Sprintf(api, name, service, name, name)
}
