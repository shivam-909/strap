package service_templates

const (
	backends = `
	package %s

	import (
		"github.com/gocql/gocql"
	)
	
	type Backends interface {
		Cassandra() *gocql.Session
	}
	`

	test = `
		package client
	`

	logic = `
		package client

		// logic contains all business logic methods for this package to expose.

		type Client interface {

		}

		type client struct {}

		func NewLogicalClient() Client {
			return &client{}
		}
	`

	api = `syntax = "proto3";
		
package %s.client.pb;
		
option go_package = "github.com/cassini-engineering/%s/%s/client/%s";
	`
)
