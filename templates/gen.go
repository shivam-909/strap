package templates

import (
	"fmt"
	"os"
)

func WriteTemplateToFile(file, tmpl string) {
	fd, err := os.OpenFile(file,
		os.O_WRONLY|os.O_TRUNC, 0666)

	if err != nil {
		panic(err)
	}

	defer fd.Close()

	_, err = fmt.Fprint(fd, tmpl)
	if err != nil {
		panic(err)
	}
}
