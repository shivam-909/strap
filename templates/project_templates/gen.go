package project_templates

import (
	"fmt"
	"go/format"
)

func GenerateMainBackends(name string) string {
	b, err := format.Source([]byte(fmt.Sprintf(mainBackends, name)))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateMain(name string) string {
	b, err := format.Source([]byte(fmt.Sprintf(mainFunc, name)))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateServer() string {
	b, err := format.Source([]byte(server))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateDatastore(name string) string {
	b, err := format.Source([]byte(fmt.Sprintf(datastore, name, "%v", "%d", "%v", "%v", "%v", "%v")))
	if err != nil {
		panic(err)
	}

	return string(b)
}

func GenerateApi(name string) string {
	b, err := format.Source([]byte(fmt.Sprintf(api, name, "%v", "%d", "%d")))
	if err != nil {
		panic(err)
	}

	return string(b)
}
