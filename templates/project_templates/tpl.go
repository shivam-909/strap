package project_templates

const (
	mainFunc = `
		package main

		func main() {
			s := server.Init()
		
			b := %v.MakeBackends()
		
			s.Serve(8000)
		}
		
	`

	mainBackends = `
		package %v

		import (
			"github.com/gocql/gocql"
		)

		//go:generate impl $PWD/$GOFILE $GOPACKAGE

		type Backends interface {
			Cassandra() *gocql.Session
		}

		type backends struct {

		}

		func MakeBackends() Backends {
			b := &backends{}

			return b
		}

		// Implement methods defined in Backends on backends
	`

	server = `
		package server 

		import (
			cassini_rpc "github.com/cassini-engineering/cassini-rpc"
		)

		func Init() *cassini_rpc.Server {
			s := cassini_rpc.NewServer()
			return s
		}
	`

	datastore = `

	package datastore

	import (
		"fmt"
		"log"
		"strings"

		"github.com/cassini-engineering/%v/api"
		"github.com/gocql/gocql"
	)

	func listHosts(h string) []string {
		hosts := []string{}
		for i := 9042; i < 9045; i++ {
			hosts = append(hosts, fmt.Sprintf("%v:%v", h, i))
		}
	
		return hosts
	}
	
	func Connect() *gocql.Session {
		cluster := gocql.NewCluster(listHosts("localhost")...)
		cluster.Keyspace = api.ServiceName
		session, err := cluster.CreateSession()
		if err != nil {
			panic(err)
		}
	
		fmt.Printf("✅ db connection established! connected to hosts: %v \n", cluster.Hosts)
		md, err := session.KeyspaceMetadata(cluster.Keyspace)
		if err != nil {
			log.Fatalf("⚠️ ‼️ keyspace %v was not found.", cluster.Keyspace)
		}
	
		fmt.Printf("🤗 cassandra session on keyspace %v is running. \n", strings.ToUpper(cluster.Keyspace))
	
		fmt.Printf("ℹ️ tables on this keyspace: %v \n", len(md.Tables))
		for _, v := range md.Tables {
			fmt.Println("⎍ ||" + strings.ToUpper(v.Name) + "||")
		}
	
		return session
	}
	
	`

	api = `
		package api

		const (
			ServiceName = "%v"
		)
		
		type Client interface { }

		type c struct {}

		func NewClient(port int32, local bool) Client {

			addr := fmt.Sprintf("http://%v-svc:%v", ServiceName, port)
			if local {
				addr = fmt.Sprintf("http://localhost:%v", port)
			}
		
		
			cl := &c{

			}
		
			return cl
		}
	`
)
