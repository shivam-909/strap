package services

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	t "github.com/cassini-engineering/strap/templates"
	templates "github.com/cassini-engineering/strap/templates/service_templates"
	"github.com/cassini-engineering/strap/util"
)

func (c *Command) Execute() error {
	return createServiceExec(c)
}

func createServiceExec(c *Command) error {
	currentDir, err := os.Getwd()
	if err != nil {
		return err
	}

	log.Printf("creating new service: %v", c.Options[1])

	name := c.Options[1]

	err = exec.Command("mkdir", name).Run()
	if err != nil {
		return err
	}

	arg := fmt.Sprintf("%s/client %s/ops %s/client/%spb", name, name, name, name)
	err = util.ExecuteSpace("mkdir", arg)
	if err != nil {
		return err
	}

	arg = fmt.Sprintf("%s/backends.go %s/client/logic.go %s/client/test.go %s/client/%spb/api.proto", name, name, name, name, name)
	fmt.Println(arg)
	err = util.ExecuteSpace("touch", arg)
	if err != nil {
		return err
	}

	t.WriteTemplateToFile(fmt.Sprintf("%s/backends.go", name), templates.GenerateBackends(name))
	t.WriteTemplateToFile(fmt.Sprintf("%s/client/test.go", name), templates.GenerateTest())
	t.WriteTemplateToFile(fmt.Sprintf("%s/client/logic.go", name), templates.GenerateLogic())
	t.WriteTemplateToFile(fmt.Sprintf("%s/client/%spb/api.proto", name, name), templates.GenerateApiProto(name, util.ServiceNameFromDir(currentDir)))

	return nil
}
