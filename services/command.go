package services

type Command struct {
	Leader  Leader
	Options []string
}

type Leader string

var (
	LeaderCreate Leader = "create"

	Leaders = map[Leader]bool{
		LeaderCreate: true,
	}
)

func ParseArgs(arg []string) *Command {
	if len(arg) < 4 {
		panic("3 arguments required to create a service")
	}

	cmd := &Command{}

	if Leaders[Leader(arg[2])] {
		cmd.Leader = Leader(arg[2])
	}

	cmd.Options = arg[2:]
	return cmd
}
