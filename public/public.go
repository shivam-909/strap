package public

type Executable interface {
	Execute() error
}
