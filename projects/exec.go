package projects

import (
	"fmt"
	"log"
	"os/exec"

	t "github.com/cassini-engineering/strap/templates"
	templates "github.com/cassini-engineering/strap/templates/project_templates"
	"github.com/cassini-engineering/strap/util"
)

func (c *Command) Execute() error {
	return createProjectExec(c)
}

func createProjectExec(c *Command) error {

	log.Println("creating project ", c.Options[1])

	name := c.Options[1]

	err := util.ExecuteSpace("mkdir", "internal internal/server internal/datastore api cmd public prod etc")
	if err != nil {
		return err
	}

	err = util.ExecuteSpace("go", fmt.Sprintf("mod init github.com/cassini-engineering/%v", name))
	if err != nil {
		return err
	}

	err = exec.Command("touch", "backends.go").Run()
	if err != nil {
		return err
	}
	t.WriteTemplateToFile("backends.go", templates.GenerateMainBackends(name))

	err = exec.Command("touch", "cmd/main.go").Run()
	if err != nil {
		return err
	}
	t.WriteTemplateToFile("cmd/main.go", templates.GenerateMain(name))

	err = exec.Command("touch", "internal/server/server.go").Run()
	if err != nil {
		return err
	}
	t.WriteTemplateToFile("internal/server/server.go", templates.GenerateServer())

	err = exec.Command("touch", "internal/datastore/connect.go").Run()
	if err != nil {
		return err
	}
	t.WriteTemplateToFile("internal/datastore/connect.go", templates.GenerateDatastore(name))

	err = exec.Command("touch", "api/api.go").Run()
	if err != nil {
		return err
	}
	t.WriteTemplateToFile("api/api.go", templates.GenerateApi(name))

	err = exec.Command("go", "get", "github.com/cassini-engineering/wtf").Run()
	if err != nil {
		return err
	}
	err = exec.Command("go", "get", "github.com/cassini-engineering/cassini-rpc").Run()
	if err != nil {
		return err
	}
	err = exec.Command("go", "get", "github.com/bufbuild/connect-go").Run()
	if err != nil {
		return err
	}
	err = exec.Command("go", "get", "google.golang.org/protobuf").Run()
	if err != nil {
		return err
	}
	err = exec.Command("go", "get", "github.com/gocql/gocql").Run()
	if err != nil {
		return err
	}

	return nil
}
