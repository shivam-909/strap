package main

import (
	"log"
	"os"

	"github.com/cassini-engineering/strap/projects"
	"github.com/cassini-engineering/strap/public"
	"github.com/cassini-engineering/strap/services"
)

func main() {
	args := os.Args
	if args == nil {
		return
	}

	var cmd public.Executable
	switch args[1] {
	case "project":
		cmd = projects.ParseArgs(args)
	case "service":
		cmd = services.ParseArgs(args)
	default:
		return
	}

	if err := cmd.Execute(); err != nil {
		log.Fatalln("failed to execute", err)
	}
}
